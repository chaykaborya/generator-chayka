<?php

namespace <%= phpNamespace %>;

use Chayka\WP\Models\PostModel;
use Chayka\WP\MVC\Controller;
use Chayka\Helpers\InputHelper;

/**
 * Class PostController is responsible for posts rendering
 *
 * @package Chayka\AShirtClub
 */
class PostController extends Controller{

	/**
	 * Controller init
	 */
	public function init(){
		// NlsHelper::load('main');
		// InputHelper::captureInput();
	}

	/**
	 * Static home page stub
	 */
	public function homeAction(){

	}

	/**
	 * Posts entry list action
	 */
	public function listAction(){
		$posts = PostModel::selectPosts();

		$this->view->assign('posts', $posts);

        $this->setupPaginationByWpQuery();
    }

	/**
	 * Posts archive output (common entry list) action
	 */
	public function archiveAction(){
		$this->forward('list');
	}

	/**
	 * Category posts list
	 */
	public function categoryAction(){
		$this->forward('list');
	}

	/**
	 * Tag posts list
	 */
	public function tagAction(){
		$this->forward('list');
	}

	/**
	 * Author posts list
	 */
	public function authorAction(){
		$this->forward('list');
	}

	/**
	 * Posts for date list
	 */
	public function dateAction(){
		$this->forward('list');
	}

	/**
	 * Search results page
	 */
	public function searchAction(){
		$this->forward('list');
	}

	/**
	 * Single post action
	 *
	 * @return bool
	 */
	public function entryAction(){
		$id = InputHelper::getParam('id');
		$slug = InputHelper::getParam('slug');
		$post = $this->loadActionPost($id, $slug, true);
		if(!$post){
			return $this->setNotFound404();
		}
		$this->view->assign('post', $post);

		return false;
	}

	/**
	 * Static page action
	 *
	 * @return bool
	 */
	public function pageAction(){
		return $this->entryAction();
	}

	/**
	 * 404 Not found action
	 */
	public function notFoundAction(){

	}

}